# vulnerable-api

Aplicação com vulnerabilidades para prova de conceito (PoC)

Aplicação baseada em https://github.com/jorritfolmer/vulnerable-api

## Rodando a aplicação

1. `git clone https://gitlab.com/poc-puc/vulnerable-api.git`
2. `pip3 install -r requirements`
3. `python ./src/bookApi.py`

## Examplos

```bash

# verifica se a aplicação está ok
curl --location --request GET 'http://localhost:5000/'

# gera um token para um usuário
curl --location --request POST 'http://localhost:5000/tokens' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username": "user1",
    "password": "pass1"
}'

# obtem dados do usuário
curl --location --request GET 'http://localhost:5000/user/1' \
--header 'X-Auth-Token: {{token}}'

# efetua uma reserva para o usuário logado
curl --location --request POST 'http://localhost:5000/book' \
--header 'X-Auth-Token: {{token}}' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "quadra de tênis 1",
    "date": "2021-12-20",
    "hour": "14h30"
}'

# confirmação com sucesso:

{
    "confirmation_code": "a3585943-7596-4318-a1c5-dd2c2774cec4",
    "message": "created reservation for book quadra de tênis 1"
}

```

