FROM node
WORKDIR /app
COPY ./src/ .
RUN apt-get update && apt-get -y install python3-pip && npm install -g python3
RUN pip3 install --no-cache-dir -r requirements.txt
RUN pytest -v
EXPOSE 8081
CMD [ "python", "./bookAPI.py" ]