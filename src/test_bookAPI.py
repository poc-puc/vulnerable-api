import flask
import unittest

class TestBook(unittest.TestCase):

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')